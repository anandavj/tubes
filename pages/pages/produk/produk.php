<?php
    session_start();
    session_write_close();
    
    $currentPage = 'produk'; // current page is about, do the same for other page

?>
								<?php include "../../../db/connection.php"; ?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<?php include "../../template/head.php"; ?>
			<!-- External CSS -->
			<!-- <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
		<meta charset="utf-8"> -->
		<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Data Produk</title>

		<!-- Google font -->
		<!-- <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet"> -->

		<!-- Bootstrap -->
			<!-- <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" /> -->

		<!-- Slick -->
		<!-- <link type="text/css" rel="stylesheet" href="css/slick.css" /> -->
		<!-- <link type="text/css" rel="stylesheet" href="css/slick-theme.css" /> -->

		<!-- nouislider -->
		<!-- <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" /> -->

		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="../../../assets/css/font-awesome.min.css">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="../../../assets/css/style.css" />

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->

	</head>

	<body style="background-color: #faf3e3">
		<!-- HEADER -->
			<!-- <?php include "../../template/navbar.php"; ?> -->
			<br> <br>
		<div id="navigation">
			<!-- container -->
			<div class="container">
				<div id="responsive-nav">
					<!-- category nav -->
					
					<!-- menu nav -->
				</div>
			</div>
			<!-- /container -->
		</div>
		<!-- /NAVIGATION -->

		<!-- BREADCRUMB -->

		<!-- /BREADCRUMB -->

		<!-- section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- ASIDE -->
				

					<!-- MAIN -->
					<div id="main" class="col-md-9">
						<!-- store top filter -->
						<div class="store-filter clearfix">
							<div class="pull-left">
								
								<div class="sort-filter">
									<span style=" font-weight: bold;">Kategori :</span>
									<select onchange="showCustomer(this.value)" class="input">
									echo	'<option value="0">Semua</option>	';

<?php // Create connection
// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM kategori";
$result = $connection->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
	echo	'<option value="'. $row["idkategori"] . '">'. $row["nama"]. '</option>	';
    }
} else {
    echo "0 results";
}
?>

										
										</select>

										<select id="subkategoridropdown" class="input">
								
										echo	'<option value="0">Semua</option>	';

										</select>
										

									<a href="#" id="searchBtn" class="main-btn icon-btn"><i class="fa fa-search"></i></a>
								</div>
							</div>
							
						</div>
						<!-- /store top filter -->

						<!-- STORE -->
						<div id="store">
							<!-- row -->
							<div class="row" id="products">
								<!-- Product Single -->
<?php // Create connection
// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM produk";
$result = $connection->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
		
	echo	'<div class="col-md-4 mb-5">
        <div class="card h-100">
          <img class="card-img-top" src="../../..'.$row['file_gambar'].'" >
          <div class="card-body" >
            <h4 class="card-title">' . $row["nama"] . '</h4>
          </div>
          <div class="card-footer">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalQuickView'.$row['idproduk'].'">Find Out More! </button>
<!-- Modal: modalQuickView -->
<div class="modal fade" id="modalQuickView'.$row['idproduk'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row justify-content-center">
        
          <div class="col-lg-7">
			<h2 class="h2-responsive product-name">
			<div class="carousel-inner" role="listbox">
			<div class="carousel-item active">
			  <img class="d-block w-100" src="../../..'.$row['file_gambar'].'"">
			</div>
	   
			</div>
	   
              <strong>' .$row["nama"].'</strong>
			</h2>
			<h5>
			<strong>' .$row["deskripsi"].'</strong>

			</h5>
			<h5>
			<strong> ' .$row["harga"].'</strong>
			</h5>
		

          
            <!-- Add to Cart -->
            <div class="card-body">
              
              <div class="text-center">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               
              </div>
            </div>
            <!-- /.Add to Cart -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
          </div>
        </div>
	  </div>'
	  ;
	  



    }
} else {
    echo "0 results";
}
$connection->close();
?>




							</div>
							<!-- /row -->
						</div>
						<!-- /STORE -->

						
						<!-- /store bottom filter -->
					</div>
					<!-- /MAIN -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /section -->



		<!-- jQuery Plugins -->
		<script src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/bootstrap.min.js"></script>
		<script src="../../../assets/js/slick.min.js"></script>
		<script src="../../../assets/js/nouislider.min.js"></script>
		<script src="../../../assets/js/jquery.zoom.min.js"></script>
		<script src="../../../assets/js/main.js"></script>
		<script>
		$('#searchBtn').click(function (){
			var xhttp;  
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$('#products').children().remove().end().append(this.responseText);

    }
  };
  var idsubkategori = $('#subkategoridropdown').val();

  xhttp.open("GET", "getproduk.php?id="+idsubkategori, true);
  xhttp.send();
			
			console.log("asd");
		})
	
function showCustomer(str) {
  var xhttp;  
  
  if (str == "0") {
	$('#subkategoridropdown').children().remove().end().append('<option value="0">Semua</option>	');
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$('#subkategoridropdown').children().remove().end().append(this.responseText);
    }
  };
  xhttp.open("GET", "getsubkategori.php?q="+str, true);
  xhttp.send();
}
// Material Select Initialization

</script>
	</body>

	</html>
