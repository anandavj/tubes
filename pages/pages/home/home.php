
<?php
    session_start();
    session_write_close();
    
    $currentPage = 'home'; // current page is about, do the same for other page
?>

<html>
    <head>
    <title>Login</title>
        <?php include "../../template/head.php"; ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    </head>
    <body >
    <?php include "../../template/navbar.php"; ?>
    <header class=" py-5 mb-5">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-lg-12">
        <h1 class="display-4 text-black mt-5 mb-2">SI PRODUK</h1>
        <p class="lead mb-5 text-black-50">Sistem Informasi Produk yang memuat pengolahan data produk, seperti stok, nama, kategori, dan lainnya.</p>
        <a class="btn btn-primary btn-lg" href="../produk/produk.php">Cek Produk &raquo;</a>

      </div>
    </div>
  </div>
</header>

<!-- Page Content -->
<!-- <div class="container"> -->
<!-- 
  <div class="row">
    <div class="col-md-8 mb-5">
      <h2>Apa yang website ini lakukan?</h2>
      <hr>
     <p>
     Website ini dapat mendata perlatan elektronik yang berada digudang beserta detailnya.
     </p> -->
    <!-- </div> -->
        <!-- <div class="col-md-4 mb-5">
        <h2>Contact Us</h2>
        <hr>
        <address>
            <strong>Start Bootstrap</strong>
            <br>3481 Melrose Place
            <br>Beverly Hills, CA 90210
            <br>
        </address>
        <address>
            <abbr title="Phone">P:</abbr>
            (123) 456-7890
            <br>
            <abbr title="Email">E:</abbr>
            <a href="mailto:#">name@example.com</a>
        </address>
        </div>   -->
  <!-- </div> -->
  <!-- /.row -->

  

<!-- </div> -->
<!-- /.container -->

<!-- Footer -->
<!-- <footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; SI PRODUK 2019</p>
  </div>
  <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>
</html> -->