<?php
    session_start();
    session_write_close();
    if($_SESSION['level'] != "admin"){
        header("location:../home.php");
    }
?>

<html>
    <head>
        <title>Pegawai</title>
        <?php include('../../template/head.php') ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    </head>
    <body style="background-color: #e4dfcf">
        <!-- <?php include "../../template/navbarAdmin.php"; ?> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 kategori rounded" style="background-color: #faf8f0">
                    <h1>Pegawai</h1>
                    <div class="tambahKategori">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createModal">Tambah Pegawai</button>
                    </div>
                    <table class="table table-striped mt-3" style="background-color: white;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Nama Lengkap</th>
                                <th>Level</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            require_once('../../../db/connection.php');
                            //create
                            if (isset($_POST['submit'])){
                                $username = $_POST['username'];
                                $email = $_POST['email'];
                                $name = $_POST['name'];
                                $password = $_POST['password'];
                                $level = $_POST['role'];
                                if (mysqli_connect_errno()){
                                    die ("Could not connect to the database: <br />".
                                    mysqli_connect_error( ));
                                }
                                $query = "SELECT * FROM pegawai WHERE username='{$username}'";
                                $query2 = "SELECT * FROM pegawai WHERE email='{$email}'";
                                $result1 = mysqli_query($connection,$query);//execute
                                $result2 = mysqli_query($connection,$query2);
                                $check = mysqli_num_rows($result1);
                                $check2 = mysqli_num_rows($result2);
                                if($check == 0 && $check2 == 0){
                                    $query3= "INSERT INTO pegawai (username,email,nama_lengkap,password,level) VALUES ('{$username}','{$email}','{$name}','{$password}','{$level}')";
                                    $result4= mysqli_query($connection,$query3);
                                    header("location:listpegawai.php");
                                }else{
                                    $error = true;
                                }
                            }
                            //update
                            if (isset($_POST['update'])){
                                $username = $_POST['username'];
                                $email = $_POST['email'];
                                $name = $_POST['name'];
                                $password = $_POST['password'];
                                $level = $_POST['role'];
                                if (mysqli_connect_errno()){
                                    die ("Could not connect to the database: <br />".
                                    mysqli_connect_error( ));
                                }
                                // $query = "SELECT * FROM pegawai WHERE username='{$username}'";
                                // $query2 = "SELECT * FROM pegawai WHERE email='{$email}'";
                                // $result1 = mysqli_query($connection,$query);//execute
                                // $result2 = mysqli_query($connection,$query2);
                                // $check = mysqli_num_rows($result1);
                                // $check2 = mysqli_num_rows($result2);
                                // $id1 = mysqli_fetch_array($result1);
                                // $idd = $_GET['id'];
                                // print_r($check);print_r($check2);
                                if($check == 0 && $check2 == 0){
                                    $query= "UPDATE pegawai SET nama_lengkap='{$name}', password='{$password}', level='{$level}' WHERE idpegawai='{$_GET['id']}'";
                                    $result= mysqli_query($connection,$query);
                                    header("location:listpegawai.php");
                                }else{
                                    $error = true;
                                }
                            }
                            //delete
                            if(isset($_GET['iddelete'])){
                                $id = $_GET['iddelete'];
                                $query = "DELETE FROM pegawai WHERE idpegawai='{$id}'";
                                $result = mysqli_query($connection,$query);
                                    header("location:listpegawai.php");
                            }
                            if (mysqli_connect_errno()){
                                die ("Could not connect to the database: <br />".
                                mysqli_connect_error( ));
                            }
                            //list
                            $query = "SELECT * FROM pegawai";
                            $result = mysqli_query($connection,$query);
                            //Asign a query
                            if (!$result){
                                die ("Could not query the database: <br />". mysqli_error($connection));
                            }
                            $i = 1;
                            while ($row = mysqli_fetch_array($result)){
                                echo '<tr>';
                                echo '<td >'.$i.'</td>';
                                echo '<td>'.$row['username'].'</td>';
                                echo '<td>'.$row['email'].'</td>';
                                echo '<td>'.$row['nama_lengkap'].'</td>';
                                echo '<td>'.$row['level'].'</td>';
                                echo '<td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="listpegawai.php?id='.$row['idpegawai'].'"><button type="button" class="btn btn-dark">Edit</button></a> <a href="listpegawai.php?iddelete='.$row['idpegawai'].'"><button type="button" class="btn btn-danger">Delete</button></a></div>
                                </td>';
                                echo '</tr>';
                                $i++;
                            }
                            echo '</tbody>';
                            echo '</table>';
                            $i = 0;
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Add -->
        <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> 
                    <div class="modal-body">
                            <form id="kategori-create" method="POST">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" placeholder="Username" required minlength="3">
                            </div>
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" required minlength="3">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Email" required minlength="3">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="password" required minlength="3">
                            </div>
                            <div>
                                <label>Role</label>
                                <select class="form-control" name="role">
                                    <option value="">--Select Role--</option>
                                    <option value="admin">Admin</option>
                                    <option value="manager">Manager</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Update -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> 
                    <div class="modal-body">
                            <form id="kategori-update" method="POST">  
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" id="pegawai-name" name="name" placeholder="Nama Lengkap" required minlength="3">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" id="pegawai-password" name="password" placeholder="password" required minlength="3">
                            </div>
                            <div>
                                <label>Role</label>
                                <select class="form-control" id="pegawai-role" name="role">
                                    <option value="">--Select Role--</option>
                                    <option value="admin">Admin</option>
                                    <option value="manager">Manager</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="update" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
<?php
    if(isset($error) && isset($_POST['submit'])){?>
        <script type="text/javascript">
            $("#createModal").modal();
        </script>
<?php
    }
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT * FROM pegawai WHERE idpegawai='{$id}'";
        $hehe = mysqli_query($connection,$query);
        $id = mysqli_fetch_array($hehe);
        ?>
        <script type="text/javascript">
            $("#editModal").modal();
            $('#pegawai-name').val('<?=$id['nama_lengkap']?>');
            $('#pegawai-password').val('<?=$id['password']?>');
            $('#pegawai-role').val('<?=$id['level']?>');

        </script>
<?php        
    }
    mysqli_close($connection);
?>
</html>