<?php
    session_start();
    session_write_close();
    if($_SESSION['level'] != "admin"){
        header("location:../home.php");
    }
?>

<html>
    <head>
        <title>Kategori</title>
        <?php include('../../template/head.php') ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    </head>
    <body style="background-color: #e4dfcf">
        <!-- <?php include "../../template/navbarAdmin.php"; ?> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 kategori rounded" style="background-color: #faf8f0">
                    <h1>Kategori</h1>
                    <div class="tambahKategori">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createModal">Tambah Kategori</button>
                    </div>
                    <table class="tabellist table table-striped" style="background-color: white">
                        <thead>                            
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            require_once('../../../db/connection.php');
                            //create
                            if (isset($_POST['submit'])){
                                $name = $_POST['name'];
                                if (mysqli_connect_errno()){
                                    die ("Could not connect to the database: <br />".
                                    mysqli_connect_error( ));
                                }
                                $query = "SELECT * FROM kategori WHERE nama='{$name}'";
                                $result = mysqli_query($connection,$query);//execute
                                $check = mysqli_num_rows($result);
                                if($check > 0){
                                    $error = true;
                                }else{
                                    $query= "INSERT INTO kategori (nama) VALUES ('$name')";
                                    $result= mysqli_query($connection,$query);
                                    header("location:listkategori.php");
                                }
                            }
                            //update
                            if (isset($_POST['update'])){
                                $name = $_POST['name'];
                                if (mysqli_connect_errno()){
                                    die ("Could not connect to the database: <br />".
                                    mysqli_connect_error( ));
                                }
                                $query = "SELECT * FROM kategori WHERE nama='{$name}'";
                                $result = mysqli_query($connection,$query);//execute
                                $check = mysqli_num_rows($result);
                                if($check > 0){
                                    header("location");
                                }else{
                                    $query= "UPDATE kategori SET nama='{$name}' WHERE idkategori='{$_GET['id']}'";
                                    $result= mysqli_query($connection,$query);
                                    header("location:listkategori.php");
                                }
                            }
                            //delete
                            if(isset($_GET['iddelete'])){
                                $id = $_GET['iddelete'];
                                $query = "DELETE FROM kategori WHERE idkategori='{$id}'";
                                $result = mysqli_query($connection,$query);
                                    header("location:listkategori.php");
                            }
                            if (mysqli_connect_errno()){
                                die ("Could not connect to the database: <br />".
                                mysqli_connect_error( ));
                            }
                            $query = "SELECT * FROM kategori";
                            $result = mysqli_query($connection,$query);
                            //Asign a query
                            if (!$result){
                                die ("Could not query the database: <br />". mysqli_error($connection));
                            }
                            $i = 1;
                            while ($row = mysqli_fetch_array($result)){
                                echo '<tr>';
                                echo '<td style="font-size: 25px">'.$i.'</td>';
                                echo '<td style="font-size: 25px">'.$row['nama'].'</td>';
                                echo '<td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="listkategori.php?id='.$row['idkategori'].'"><button type="button" class="btn btn-dark">Edit</button></a> <a href="listkategori.php?iddelete='.$row['idkategori'].'"><button type="button" class="btn btn-danger">Delete</button></a></div> 
                                    </td>';

                                echo '</tr>';
                                $i++;
                            }
                            echo '</tbody>';
                            echo '</table>';
                            $i = 0;
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Add -->
        <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> 
                    <div class="modal-body">
                            <form id="kategori-create" method="POST">
                            <div class="form-group">
                                <label>Nama Kategori</label>
                                <input type="text" class="form-control" name="name" placeholder="Nama kategori" required minlength="3">
                            </div><!-- 
                            <div>
                                <label>Role</label>
                                <select class="form-control" name="role">
                                    <option value="">--Select Role--</option>
                                    <option value="admin">Admin</option>
                                    <option value="pegawai">Pegawai</option>
                                </select>
                            </div> -->
                            <div class="modal-footer">
                                <input type="submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Update -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> 
                    <div class="modal-body">
                            <form id="kategori-update" method="POST">
                            <div class="form-group">
                                <label>Nama Kategori</label>
                                <input type="text" class="form-control" id="kategori-name" name="name" placeholder="Nama kategori" required minlength="3">
                            </div><!-- 
                            <div>
                                <label>Role</label>
                                <select class="form-control" name="role">
                                    <option value="">--Select Role--</option>
                                    <option value="admin">Admin</option>
                                    <option value="pegawai">Pegawai</option>
                                </select>
                            </div> -->
                            <div class="modal-footer">
                                <input type="submit" name="update" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
<?php
    if(isset($error)){?>
        <script type="text/javascript">
            $("#createModal").modal();
        </script>
<?php
    }
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT * FROM kategori WHERE idkategori='{$id}'";
        $hehe = mysqli_query($connection,$query);
        $id = mysqli_fetch_array($hehe);
        ?>
        <script type="text/javascript">
            $("#editModal").modal();
            $('#kategori-name').val('<?=$id['nama']?>');

        </script>
<?php        
    }
    mysqli_close($connection);
?>
</html>