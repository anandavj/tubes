<?php
    session_start();
    session_write_close();
    if($_SESSION['level'] != "admin"){
        header("location:../home/home.php");
    }
?>

<html>
    <head>
        <title>Admin Homepage</title>
        <?php include('../../template/head.php') ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    </head>
    <body style="background-color: #e4dfcf">
        <!-- <?php include "../../template/navbarAdmin.php"; ?> -->
        <div class="container">
            <div class="row" style="margin-top: 70px">
                <div class="col-lg-10" ></div>
            </div>
            <div class="row vh-75">
                <div class="col-lg-6 d-flex justify-content-center align-items-center">
                    <a href="admin2.php">
                        <div class="box-menu text-center shadow-lg" style="background-color: #faf8f0; border: solid 4px #e44652;">
                            <img class="img-fluid" src="../../../assets/img/product-logo.jpg" alt="">
                            <h2>Produk</h2>      
                        </div>
                    </a>
                </div>
                <div class="col-lg-6 d-flex justify-content-center align-items-center">
                    <a href="listPegawai.php">
                        <div class="box-menu text-center shadow-lg" style="background-color: #faf8f0; border: solid 4px #e44652;">
                            <img class="img-fluid" src="../../../assets/img/employee-logo.png" alt="">       
                            <h2>Pegawai</h2>     
                        </div>
                    </a>
                </div>
            </div>
            <!-- <div class="col-lg-12 d-flex justify-content-center align-items-center logOut">
            </div> -->
        </div>
    </body>
</html>