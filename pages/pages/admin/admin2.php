<?php
    session_start();
    session_write_close();
    if($_SESSION['level'] != "admin"){
        header("location:../home/home.php");
    }
?>

<html>
    <head>
        <title>Admin Homepage</title>
        <?php include('../../template/head.php') ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    </head>
    <body style="background-color: #e4dfcf">
        <!-- <?php include "../../template/navbarAdmin.php"; ?> -->
        <div class="container">
            <div class="row" style="margin-top: 70px">
                <div class="col-lg-10" ></div>
            </div>
            <div class="row vh-100">
                <div class="col-lg-4 d-flex justify-content-center align-items-center" >
                    <a href="listproduk.php">
                        <div class="bmcuk rounded text-center shadow-lg" style="background-color: #faf8f0; border: solid 3px #e44652;">
                            <img class="img-fluid" src="../../../assets/img/product-logo.jpg" alt="">
                            <h3>Produk</h3>      
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 d-flex justify-content-center align-items-center text-center">
                    <a href="listKategori.php">
                        <div class="bmcuk rounded text-center shadow-lg" style="background-color: #faf8f0; border: solid 3px #e44652;">
                            <img class="img-fluid" src="../../../assets/img/kategori.png" alt="">
                            <h3>Kategori</h3>      
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 d-flex justify-content-center align-items-center text-center">
                    <a href="listSubKategori.php">
                        <div class="bmcuk rounded text-center shadow-lg" style="background-color: #faf8f0; border: solid 3px #e44652;">
                            <img class="img-fluid" src="../../../assets/img/subkategori.png" alt="">
                            <h3>Sub Kategori</h3>      
                        </div>
                    </a>
                </div>
            </div>
            <!-- <div class="col-lg-12 d-flex justify-content-center align-items-center logOut">
            </div> -->
        </div>
    </body>
</html>