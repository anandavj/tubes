<?php
    session_start();
    session_write_close();
    if($_SESSION['level'] != "admin"){
        header("location:../home.php");
    }
?>

<html>
    <head>
    
    <script>
        function showCustomer(str) {
            console.log(str);
  var xhttp;  
  
  if (str == "0") {
	$('#subkategoridropdown').children().remove().end().append('<option value="0">Semua</option>	');
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		$('#subkategoridropdown').children().remove().end().append(this.responseText);
    }
  };
  xhttp.open("GET", "getsubkategori.php?q="+str, true);
  xhttp.send();
}
        </script>
        <title>List Produk</title>
        <?php include('../../template/head.php') ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    </head>
    <body style="background-color: #e4dfcf">
        <!-- <?php include "../../template/navbarAdmin.php"; ?> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 kategori rounded" style="background-color: #faf8f0">
                    <h1>Produk</h1>
                    <div class="tambahKategori">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createModal">Tambah Produk</button>
                    </div>
                    <table class="tabellist table table-striped" style="background-color: white">
                        <thead>                            
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            require_once('../../../db/connection.php');
                            //create
                            if (isset($_POST['submit'])){
                                $name = $_POST['name'];
                                // print_r($_SESSION['id_pegawai']);
                                $subkategori = $_POST['subkategoriOption'];
                                $deskripsi = $_POST['deskripsi'];
                                $kategori = $_POST['kategoriOption'];
                                $harga = $_POST['harga'];
                                $id_pegawai=$_SESSION['id_pegawai'];
                                $img = $_FILES["userImage"]["name"];
                                $loc = $_FILES['userImage']['tmp_name'];
                                $path = "../../../assets/img/";
                                move_uploaded_file($loc,$path.$img);
                                $dir= "/assets/img/".$img;
                                if (mysqli_connect_errno()){
                                    die ("Could not connect to the database: <br />".
                                    mysqli_connect_error( ));
                                }
                                // print_r($_POST);
                                $query = "SELECT * FROM produk WHERE nama='{$name}'";
                                $result = mysqli_query($connection,$query);//execute
                                $check = mysqli_num_rows($result);

                                if($check > 0){
                                    $error = true;

                                }else{
                                    $query= "INSERT INTO produk (nama,idsubkategori,idkategori,deskripsi,harga,idpegawai,file_gambar) VALUES ('{$name}','{$subkategori}','{$kategori}','{$deskripsi}','{$harga}','{$id_pegawai}','{$dir}')";
                                    $result= mysqli_query($connection,$query);
                                }
                                
                            }
                            //update
                            if (isset($_POST['update'])){
                                // print_r($_POST);
                                $name = $_POST['name'];
                                $idsubkategori = $_POST['subkategoriOption'];
                                $deskripsi = $_POST['deskripsi'];
                                $kategori = $_POST['kategoriOption'];
                                $harga = $_POST['harga'];
                                $id_pegawai=$_SESSION['id_pegawai'];
                                if (mysqli_connect_errno()){
                                    die ("Could not connect to the database: <br />".
                                    mysqli_connect_error( ));
                                }
                                $query = "SELECT * FROM produk WHERE nama='{$name}'";
                                $result = mysqli_query($connection,$query);//execute
                                $check = mysqli_num_rows($result);
                                $query= "UPDATE produk SET nama='{$name}', harga='{$harga}', deskripsi='{$deskripsi}', idsubkategori='{$idsubkategori}', idkategori='{$kategori}' WHERE idproduk='{$_GET['id']}'";
                                    $result= mysqli_query($connection,$query);
                                    print_r($result);
                                    // header("location:listproduk.php");
                                // if($check > 0){
                                //     header("location");
                                // }else{
                                //     // print_r("test");
                                    
                                // }
                            }
                            //delete
                            if(isset($_GET['iddelete'])){
                                $id = $_GET['iddelete'];
                                $query = "DELETE FROM produk WHERE idproduk='{$id}'";
                                $result = mysqli_query($connection,$query);
                                    header("location:listproduk.php");
                            }
                            if (mysqli_connect_errno()){
                                die ("Could not connect to the database: <br />".
                                mysqli_connect_error( ));
                            }
                            $query = "SELECT * FROM produk";
                            $result = mysqli_query($connection,$query);
                            //Asign a query
                            if (!$result){
                                die ("Could not query the database: <br />". mysqli_error($connection));
                            }
                            $i = 1;
                            while ($row = mysqli_fetch_array($result)){
                                echo '<tr>';
                                echo '<td style="font-size: 25px">'.$i.'</td>';
                                echo '<td style="font-size: 25px">'.$row['nama'].'</td>';
                                echo '<td style="font-size: 25px">'.$row['harga'].'</td>';
                                echo '<td style="font-size: 25px">'.$row['deskripsi'].'</td>';
                                echo '<td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="listproduk.php?id='.$row['idproduk'].'"><button type="button" class="btn btn-dark">Edit</button></a> <a href="listproduk.php?iddelete='.$row['idproduk'].'"><button type="button" class="btn btn-danger">Delete</button></a></div> 
                                    </td>';

                                echo '</tr>';
                                $i++;
                            }
                            echo '</tbody>';
                            echo '</table>';
                            $i = 0;
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Add -->
        <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> 
                    <div class="modal-body">
                            <form  onsubmit="alert('success');" id="produk-create" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" class="form-control" name="name" placeholder="Nama Barang" required minlength="3">
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="text" class="form-control" name="harga" placeholder="Harga Barang" required minlength="3">
                            </div>
                             <div>
                                <label>Kategori</label>
                                <select onchange="showCustomer(this.value)" class="form-control" name="kategoriOption">
                                    <option  value="0">--Pilih Kategori--</option>
                                    <?php
                                        $query = " SELECT * FROM kategori ORDER BY idkategori";
                                        $result = mysqli_query($connection,$query);
                                        while($row=mysqli_fetch_array($result)){
                                            ?>
                                            <option value="<?=$row['idkategori']?>"><?=$row['nama']?></option><?php
                                        }?>
                                </select>
                            </div>
                             <div>
                                <label>SubKategori</label>
                                <select id="subkategoridropdown" class="form-control" name="subkategoriOption">
                                    <option value="">--Pilih SubKategori--</option>
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" placeholder="Deksripsi" required minlength="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Gambar Produk</label></br>
                                <input name="userImage" type="file" class="inputFile" />
                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Update -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> 
                    <div class="modal-body">
                            <form id="produk-update" method="POST">
                            <div class="form-group">
                                <label>Nama Barang</label>
                                <input type="text" id="produk-nama" class="form-control" name="name" placeholder="Nama kategori" required minlength="3">
                            </div>
                            <div class="form-group">
                                <label>Harga</label>
                                <input type="text" id="produk-harga" class="form-control" name="harga" placeholder="Nama kategori" required minlength="3">
                            </div>
                             <div>
                                <label>Kategori</label>
                                <select class="form-control" id="produk-kategori" name="kategoriOption">
                                    <option value="">--Pilih Kategori--</option>
                                    <?php
                                        $query = " SELECT * FROM kategori ORDER BY idkategori";
                                        $result = mysqli_query($connection,$query);
                                        while($row=mysqli_fetch_array($result)){
                                            ?>
                                            <option value="<?=$row['idkategori']?>"><?=$row['nama']?></option><?php
                                        }?>
                                </select>
                            </div>
                             <div>
                                <label>SubKategori</label>
                                <select class="form-control" id="produk-subkategori" name="subkategoriOption">
                                    <option value="">--Pilih Kategori--</option>
                                    <?php
                                        $query = " SELECT * FROM subkategori ORDER BY idsubkategori";

                                        $result = mysqli_query($connection,$query);
                                        while($row=mysqli_fetch_array($result)){
                                            ?>
                                            <option value="<?=$row['idsubkategori']?>"><?=$row['nama']?></option><?php
                                        }?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" id="produk-deskripsi" name="deskripsi" placeholder="Deksripsi" required minlength="3"></textarea>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="update" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
<?php
    if(isset($error)){?>
        <script type="text/javascript">
            $("#createModal").modal();
        </script>
<?php
    }
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT * FROM produk WHERE idproduk='{$id}'";
        $hehe = mysqli_query($connection,$query);
        $id = mysqli_fetch_array($hehe);
        ?>

        <script type="text/javascript">

            $("#editModal").modal();
            $('#produk-nama').val('<?=$id['nama']?>');
            $('#produk-harga').val('<?=$id['harga']?>');
            $('#produk-deskripsi').val('<?=$id['deskripsi']?>');
            $('#produk-kategori').val('<?=$id['idkategori']?>');
            $('#produk-subkategori').val('<?=$id['idsubkategori']?>');
        </script>
<?php        
    }
    mysqli_close($connection);
?>
</html>