<?php
    session_start();
    session_write_close();
    if(!empty($_SESSION['level'])){
        if($_SESSION['level'] == "admin"){
            header("location:admin/admin.php");
        }else{
            if($_SESSION['level'] == "pegawai"){
            header("location:manager/manager.php");
            }
        }
    }

?>

<html>
    <head>
    <title>Login</title>
        <?php include "../template/head.php"; ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
    </head>
    <body style="background-image: url(../../assets/img/bg1.jpg); background-size: cover;">
        <div class="container">
            <div class="row mt-5 justify-content-md-center">
                <div class="col-lg-7"></div>

                <div class="col-12 col-lg-5 rounded p-5 text-center shadow-lg rounded" style="background: #faf8f0; border: solid 1px #e44652;">
                    <form action="../../controller/login.php" method="POST">
                    <a href="../pages/home/home.php"><img class="mb-5" src="../../assets/img/logo.png" alt="" width="150" height="150"></a>
                        <h1 class="h3 mb-3 font-weight-normal">LOGIN ADMIN</h1>
                        <input type="text" name="username" class="form-control" placeholder="Username">
                        <br>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <button type="submit" class="btn btn-primary btn-block mt-3">Sign In</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>