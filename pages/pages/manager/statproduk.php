    <?php
    session_start();
    session_write_close();
    if($_SESSION['level'] != "manager"){
        header("location:../produk/produk.php");
    }
?>

<html>
    <head>
        <title>Manager Homepage</title>
        <script src="../../../assets/js/Chart.min.js"></script>
	<script src="../../../assets/js/utils.js"></script>
	<style>
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	</style>
        <?php include('../../template/head.php') ?>
        <!-- External CSS -->
        <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    </head>
    <body style="background-color: #e4dfcf">
        <!-- <?php include "../../template/navbarManager.php"; ?> -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 kategori rounded" style="background-color: #faf8f0">
                    <h1>Produk</h1>
                    <table class="table table-striped" style="background-color: white">
                        <thead>                            
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>SubKategori</th>
                                
                                <th>Nama</th>
                                <th>Harga</th>
                                
                                <th>Deskripsi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            require_once('../../../db/connection.php');
                            //create
                          
                            if (mysqli_connect_errno()){
                                die ("Could not connect to the database: <br />".
                                mysqli_connect_error( ));
                            }
                            $query3 = "SELECT * FROM kategori ORDER BY idkategori";
                            $query2 = "SELECT * FROM subkategori ORDER BY idkategori, idsubkategori";
                            $result3 = mysqli_query($connection,$query3);
                            $result2 = mysqli_query($connection,$query2);

                            //Asign a query
                            if (!$result3){
                                die ("Could not query the database: <br />". mysqli_error($connection));
                            }
                            $i = 0;
                            $ii=0;
                            while($row = mysqli_fetch_array($result3)){
                                echo "<tr>";
                                $idkategori = $row['idkategori'];
                                $adaberapasubkategoridikategoritersebut = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM subkategori WHERE subkategori.idkategori= $idkategori"));
                                $i = 0;
                                $ii++;
                                $totalkategori = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM produk INNER JOIN subkategori ON produk.idsubkategori = subkategori.idsubkategori INNER JOIN kategori ON kategori.idkategori = subkategori.idkategori WHERE kategori.idkategori = $idkategori"));
                                if($totalkategori ==0) continue;
                                echo "<td rowspan='$totalkategori'>$ii</td>";
                                echo "<td rowspan='$totalkategori'>{$row["nama"]} </td>";
                                while($row2 = mysqli_fetch_array($result2)){
                                    $idsubkategori = $row2['idsubkategori'];

                                    $a = mysqli_num_rows(mysqli_query($connection, "SELECT * FROM produk WHERE produk.idsubkategori= $idsubkategori"));

                                    if($i == 0){
                                        echo "<td rowspan='{$a}'>{$row2["nama"]}</td>";
                                    }else{
                                        echo '<tr>';
                                        echo "<td rowspan='{$a}'>{$row2["nama"]}</td>";
                                    }
                                    $i++;
                                 
                                    $j = 0;
                                    $query1 = "SELECT * FROM produk WHERE idsubkategori= $idsubkategori";
                                    $result1 = mysqli_query($connection,$query1);

        

                                    while($row3 = mysqli_fetch_array($result1)){   


                        
                                        if($j == 0){
                                            echo '<td>'.$row3['nama'].'</td>';
                                            echo '<td>'.$row3['harga'].'</td>';

                                            echo '<td>'.$row3['deskripsi'].'</td>';
                                            echo '</tr>';
                                        }else{
                                            echo '<tr>';
                                            echo '<td>'.$row3['nama'].'</td>';
                                            echo '<td>'.$row3['harga'].'</td>';

                                            echo '<td>'.$row3['deskripsi'].'</td>';
                                            echo '</tr>';
                                        }
                                        $j++;
                                        if($j == $a)break;
                                       
                                        
                                    }
                                    if($i == $adaberapasubkategoridikategoritersebut)break;
                                }
                            }
                            echo '</tbody>';
                            echo '</table>';
                            $i = 0;
                        ?>
                    </table>
                </div>    
            </div>
            <div class="row">
                <!-- <div class="col-lg-12 kategori rounded" style="background-color: #faf8f0"> -->
                <div class="col-xl-12 col-md-12 col-sm-8 kategori mb-5 d-flex align-items-center justify-content-center " >
                        <div class="header"><h1 style="text-align: center">Grafik Kategori</h1></div>
                        <div class="chart-container rounded" style="position: relative; height:100vh; width:80vw;background-color: #faf8f0">
                            <canvas id="myChart"></canvas>    
                        </div>
                    </div>
            </div>

        </div>
	</div>
    <div class="row">
        
        
      
     
    </div>  

<?php
    $query = "SELECT p.*, k.nama as nama_kategori, COUNT(k.idkategori) as jumlahkategori
        FROM produk as p 
        join subkategori as sb on sb.idsubkategori = p.idsubkategori 
        join kategori as k on k.idkategori = p.idkategori
        GROUP BY nama_kategori    
    ";    


    $result = mysqli_query($connection,$query);
    $label = array();
    $data = array();
    while($row = mysqli_fetch_assoc($result)){
        $label[] =$row["nama_kategori"];
        $data[] = $row["jumlahkategori"];
        
        
    }

    function js_str($s)
    {
        return '"' . addcslashes($s, "\0..\37\"\\") . '"';
    }

    function js_array($array)
    {
        $temp = array_map('js_str', $array);
        return '[' . implode(',', $temp) . ']';
    }
?>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?= js_array($label) ?>,
        datasets: [{
            label: 'Rekapitulasi produk berdasarkan kategori',
            data: <?= js_array($data)?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
myChart.canvas.parentNode.style.height = '370px';
myChart.canvas.parentNode.style.width = '800px';
</script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <script src="" async defer></script>
    

    </body>
</html>