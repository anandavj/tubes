
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top shadow-lg" style="background-color: #043353">
    <div class="container">
      <a class="navbar-brand" href="../home" style="color: #faf3e3">
        <img src="../../../assets/img/logo2.png" alt="" width="40" height="40" >
        SI PRODUK
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive" style="color: #faf3e3">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php if($currentPage =='home'){echo 'active';}?>">
          <a class="nav-link" href="../home">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item <?php if($currentPage =='produk'){echo 'active';}?>">
          <a class="nav-link" href="../produk/produk.php">Data Produk</a>
          </li>
          <li class="nav-item <?php if($currentPage =='login'){echo 'active';}?>">
          <a class="nav-link" href="../login.php  ">Login</a>
          </li>
       
        </ul>
      </div>
    </div>
  </nav>
