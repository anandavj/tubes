
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top shadow-md" style="background-color: #043353">
    <div class="container">
      <a class="navbar-brand" href="../manager/manager.php" style="color: #faf3e3">
        <img src="../../../assets/img/logo2.png" alt="" width="40" height="40" >
        Sitem Informasi PRODUK
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive" style="color: #faf3e3">
        <ul class="navbar-nav ml-auto">
          <form action="../../../controller/logout.php"><button type="submit" class="btn btn-danger">Log Out</button></form>
        </ul>
      </div>
    </div>
  </nav>
